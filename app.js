'use strict';

const port = process.env.PORT || 3000;

require('./models/db');
const Recipes = require('./controllers/recipes');
const Clients = require('./controllers/clients');
const Orders = require('./controllers/orders');
const Users = require('./controllers/users');
const express = require('express');
const app = express();

const server = app.listen(port, () => console.log('Express started at port ' + port));

const io = require('socket.io')(server);

const path = require('path');
const favicon = require('serve-favicon');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res, next) {
    res.sendFile(path.join(__dirname, '/public/src/cafeapp/index.html'));
});
app.get('/kitchen', function (req, res, next) {
    res.sendFile(path.join(__dirname, '/public/src/kitchenapp/index.html'));
});

io.on('connection', function (socket) {
    console.log(socket.id + ' Connected user...');

    socket.on('login', function (user, fn) {
        console.log(socket.id + ' Trying login user with e-mail: ' + user.email);
        Users.loginUser(user, fn, socket);
    });

    socket.on('login:kitchen', function (request, fn) {
        console.log(socket.id + ' Login kitchen-user');
        Users.loginKitchenUser(fn, socket);
        console.log('Users count: ' + Users.count());
    });

    socket.on('recharge', function (request, fn) {
        console.log(socket.id + ' Recharge balance for user id ' + request._id);
        Clients.changeBalance(request, res => fn(res));
    });

    socket.on('recipes:list', function (request, fn) {
        console.log(socket.id + ' Get recipe list');
        Recipes.list(undefined, fn);
    });

    socket.on('recipes:get', function (request, fn) {
        console.log(socket.id + ' Get recipe detail ' + request._id);
        Recipes.getOne(request, fn);
    });

    socket.on('orders:post', function (request, fn) {
        console.log(socket.id + ' Order by user id ' + request.client);
        Orders.postOrder(request, fn);
    });

    socket.on('orders:get', function (request, fn) {
        console.log(socket.id + ' Get order list');
        Orders.list(request, fn);
    });

    socket.on('orders:change', function (request, fn) {
        console.log(socket.id + ' Change order ' + request._id + ' to ' + request.state);
        Orders.changeState(request, fn);
    });

    socket.on('disconnect', function () {
        console.log(socket.id + ' Disconnected user');
        Users.delete(socket.id);
        console.log('Users count ' + Users.count());
    });
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
