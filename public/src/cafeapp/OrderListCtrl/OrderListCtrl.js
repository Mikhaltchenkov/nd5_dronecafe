'use strict';

angular
    .module('CafeApp')
    .controller('OrderListCtrl', function ($scope, OrderService, ProfileService) {
        $scope.OrderListLoaded = false;
        $scope.notLoggedMessage = false;
        $scope.orders = [];

        if (ProfileService.getState()) {

            OrderService.getOrderList({"client": ProfileService.getProfile()._id}, function (err, res) {
                if (!err) {
                    $scope.orders = res;
                    $scope.orderListLoaded = true;
                } else {
                    Materialize.toast('Произошла ошибка: ' + err.message, 5000, 'red');
                }
            });
        } else {
            $scope.notLoggedMessage = true;
            $scope.OrderListLoaded = true;
        }

        $scope.isNew = function (order) {
            let date = new Date();
            date.setMinutes(date.getMinutes() - 5);
            let dateTime = date.getTime();
            return (Date.parse(order.ordered) >= dateTime || Date.parse(order.changed) >= dateTime)
        };
    });