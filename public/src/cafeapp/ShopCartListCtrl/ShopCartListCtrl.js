'use strict';

angular
    .module('CafeApp')
    .controller('ShopCartListCtrl', function($scope,ProfileService, ShopCartService) {
        $scope.shopCart = ShopCartService.getItems();
        $scope.shopCartAmount = 0;

        $scope.calcCartAmount = function () {
            $scope.shopCartAmount = 0;
            for (let item in this.shopCart) {
                this.shopCartAmount += this.shopCart[item].quantity * this.shopCart[item].item.price;
            }
            return this.shopCartAmount;
        };

        $scope.removeFromCart = function (id) {
            Materialize.toast('Позиция удалена из корзины', 3000);
            ShopCartService.removeItem(id);
        };

        $scope.clearCart = function () {
            ShopCartService.clear();
            Materialize.toast('Корзина очищена', 3000);
        };

        $scope.orderCart = function () {
            if (ProfileService.getState()) {
                ShopCartService.postOrder(ProfileService.getProfile()._id, function (err, res) {
                    if (!err) {
                        Materialize.toast('Заказ успешно оформлен, ожидайте!', 3000);
                    } else {
                        Materialize.toast(err.message, 3000);
                    }
                });
            } else {
                Materialize.toast('Для оформления заказа необходимо зарегистрироваться!', 3000);
            }
        };

        $scope.isEmptyCart = function() {
            for(var prop in this.shopCart) {
                if(this.shopCart.hasOwnProperty(prop))
                    return false;
            }
            return true;
        };

        $scope.getIncompleteSumBalance = function() {
            return ShopCartService.calcCartAmount() - ProfileService.getProfile().balance;
        };

    });