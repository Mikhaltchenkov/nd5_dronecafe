'use strict';

angular
    .module('CafeApp')
    .component('profileComponent', {
        templateUrl: 'src/cafeapp/ProfileComponent/ProfileComponentTemplate.html',
        controller: function($scope, ProfileService) {
            const RECHARGE_VALUE = 100;
            $scope.profile = {};
            $scope.loggedIn = false;
            this.login = function (profile) {
                ProfileService.login(profile.name, profile.email, function (err, res) {
                    if (!err) {
                        $scope.profile = res;
                        $scope.loggedIn = true;
                    } else {
                        Materialize.toast(err.message, 3000);
                    }
                });
            };
            this.recharge = function () {
                ProfileService.recharge(RECHARGE_VALUE, function (err, res) {
                    if (!err) {
                        Materialize.toast('Ваш баланс пополнен на ' + RECHARGE_VALUE + ' Q', 3000);
                    } else {
                        Materialize.toast(err.message, 3000);
                    }
                });
            };
        }
    });