'use strict';

angular
    .module('CafeApp')
    .component('shopCartComponent', {
        templateUrl: 'src/cafeapp/ShopCartComponent/ShopCartComponentTemplate.html',
        controller: function(ShopCartService) {
            this.cartItems = ShopCartService.getItems();

            this.removeFromCart = function(recipeId) {
                ShopCartService.removeItem(recipeId);
            };

            this.calcCartItems = function () {
                let items = 0;
                for (let item in this.cartItems) {
                    items += this.cartItems[item].quantity;
                }
                return items;
            };
        }
    });