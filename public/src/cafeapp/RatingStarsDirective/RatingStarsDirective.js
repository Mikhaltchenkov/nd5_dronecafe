'use strict';

angular
    .module('CafeApp')
    .directive('ratingStars', function () {
        return {
            scope: {
                thisRating: '=rating'
            },
            templateUrl: "src/cafeapp/RatingStarsDirective/RatingStarsTemplate.html"
        };
    });