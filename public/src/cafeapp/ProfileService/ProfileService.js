'use strict';

angular
    .module('CafeApp')
    .factory('ProfileService', function(SocketService) {

        let profile = {};
        let loggedIn = false;

        SocketService.on('balance:changed', function (data) {
            profile.balance = data;
        });

        return {
            login(name, email, fn) {
                if (!name || !email) {
                    fn({'message': 'Поля Имя и E-Mail должны быть заполнены!'});
                } else {
                    if (email.match(/^[A-Za-z0-9_\.]+@[A-Za-z0-9_]+\.[A-Za-z]/i)) {
                        SocketService.emit('login', {'name': name, 'email': email}, function (res) {
                            if (res.code == 200 || res.code == 201) {
                                profile = res.result;
                                loggedIn = true;
                                fn(undefined, res.result);
                            } else {
                                fn(res);
                            }
                        });
                    } else {
                        fn({'message': 'Поле E-Mail имеет не корректное значение!'});
                    }
                }
            },
            getState()  {
                return loggedIn;
            },
            getProfile() {
                return profile;
            },
            recharge(value, fn) {
                if (loggedIn) {
                    profile.balance = value;
                    SocketService.emit('recharge', { "_id": profile._id, "sum": value }, function (res) {
                        if (res.code == 200) {
                            profile.balance = res.result.balance;
                            fn(undefined, res.result);
                        } else {
                            fn(res);
                        }
                    });
                } else {
                    fn({'message':'User not logged in'});
                }
            }
        };

    });
