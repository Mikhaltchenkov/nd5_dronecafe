'use strict';

angular
    .module('CafeApp')
    .factory('ShopCartService', function (SocketService) {

        const state = {
            cartItems: {}
        };

        return {
            getItems()  {
                return state.cartItems;
            },
            addItem(item) {
                state.cartItems[item._id] = {
                    item: item,
                    quantity: state.cartItems[item._id] ? state.cartItems[item._id].quantity + 1 : 1
                };
            },
            removeItem(itemId) {
                delete state.cartItems[itemId];
            },
            clear() {
                for (let item in state.cartItems) {
                    delete state.cartItems[item];
                }
            },
            postOrder(clientId, cb) {
                // this feature is not implemented
                let order = {
                    'client': clientId,
                    'items': []
                };

                for (let item in state.cartItems) {
                    order.items.push({'id': item, 'quantity': state.cartItems[item].quantity});
                }

                SocketService.emit('orders:post', order, res => {
                    if (res.code == 200) {
                        // clear();
                        this.clear();
                        cb(undefined, res.result);
                    } else {
                        cb(res);
                    }
                });
            },
            calcCartAmount() {
                let amount = 0;
                for (let item in state.cartItems) {
                    amount += state.cartItems[item].quantity * state.cartItems[item].item.price;
                }
                return amount;
            }
        };

    });
