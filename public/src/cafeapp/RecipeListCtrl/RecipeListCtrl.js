'use strict';

angular
    .module('CafeApp')
    .controller('RecipeListCtrl', function ($scope, RecipeService, ShopCartService) {
        $scope.Query = '';

        $scope.recipeListLoaded = true;//RecipeService.getRecipeListState();
        $scope.recipes = RecipeService.getRecipeList();

        $scope.addToCart = function (item) {
            ShopCartService.addItem(item);
            Materialize.toast('Блюдо "' + item.title + '" добавлено в корзину', 3000);
        };
    });