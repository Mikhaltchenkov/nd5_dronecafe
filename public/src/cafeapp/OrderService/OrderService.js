'use strict';

angular
    .module('CafeApp')
    .factory('OrderService', function (SocketService) {

        let orders = [];
        let thereIsNews = false;

        SocketService.on('order:deleted', function (data) {
            let num = orders.findIndex(el => el._id == data);
            if (num != -1) {
                orders.splice(num, 1);
            }
        });

        SocketService.on('order:changed', function (data) {
            let found = orders.reduce((res, item) => {
                if (item._id === data._id) {
                    item.state = data.state;
                    item.changed = data.changed;
                    return true;
                } else {
                    return res || false;
                }
            }, false);
            if (!found) {
                orders.push(data);
            }
            thereIsNews = true;
        });

        return {
            getOrderList(req, cb) {
                SocketService.emit('orders:get', req, function (res) {
                    if (res.code == 200) {
                        orders = res.result;
                        cb(undefined, orders);
                    } else {
                        cb(res);
                    }
                });
            },
            getIsThereNews() {
                return thereIsNews;
            }
        }
    });