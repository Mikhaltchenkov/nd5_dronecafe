'use strict';

angular
    .module('CafeApp')
    .factory('RecipeService', function (SocketService) {

        const recipes = {};

        SocketService.emit('recipes:list', {}, function (res) {
            if (res.code == 200) {
                recipes.loaded = true;
                recipes.items = res.result;
            }
        });

        return {
            getRecipeList() {
                return recipes;
            },
            getRecipeListState() {
                return recipes.loaded;
            },
            getRecipe(id, cb) {
                SocketService.emit('recipes:get', {'_id': id}, function (res) {
                    if (res.code == 200) {
                        cb(undefined, res.result);
                    } else {
                        cb(res);
                    }
                });
            }
        }
    });