var cafeApp = angular.module('CafeApp', ['ngRoute', 'ngResource']);

angular.module('CafeApp')
    .config(['$routeProvider',
        function config($routeProvider) {
            $routeProvider
            .when('/recipes/:recipeId', {
                templateUrl: 'src/cafeapp/RecipeDetailCtrl/RecipeDetailTemplate.html',
                controller: 'RecipeDetailCtrl'
            })
            .when('/recipes', {
                templateUrl: 'src/cafeapp/RecipeListCtrl/RecipeListTemplate.html',
                controller: 'RecipeListCtrl'
            })
            .when('/orders', {
                templateUrl: 'src/cafeapp/OrderListCtrl/OrderListTemplate.html',
                controller: 'OrderListCtrl'
            })
            .when('/cart', {
                templateUrl: 'src/cafeapp/ShopCartListCtrl/ShopCartListTemplate.html',
                controller: 'ShopCartListCtrl'
            });
            $routeProvider.otherwise({redirectTo: '/recipes'});

        }]);
