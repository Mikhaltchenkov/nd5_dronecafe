'use strict';

angular
    .module('CafeApp')
    .controller('RecipeDetailCtrl', function ($scope, $routeParams, RecipeService, ShopCartService) {
        $scope.recipeLoaded = false;
        $scope.recipe = {};

        RecipeService.getRecipe($routeParams['recipeId'], function (err, res) {
           if (!err) {
               $scope.recipe = res;
               $scope.recipeLoaded = true;
           }
        });
        $scope.addToCart = function (item) {
            ShopCartService.addItem(item);
            Materialize.toast('Блюдо "' + item.title + '" добавлено в корзину', 3000);
        };
    });