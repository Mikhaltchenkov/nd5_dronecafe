var kitchenApp = angular.module('KitchenApp', ['ngRoute', 'ngResource']);

angular.module('KitchenApp')
    .config(['$routeProvider',
        function config($routeProvider) {
            $routeProvider.
            when('/orders', {
                templateUrl: 'src/kitchenapp/OrderListCtrl/OrderListTemplate.html',
                controller: 'OrderListCtrl'
            });
            $routeProvider.otherwise({redirectTo: '/orders'});

        }]);
