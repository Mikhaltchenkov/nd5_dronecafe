'use strict';

angular
    .module('KitchenApp')
    .controller('OrderListCtrl', function ($scope, OrderService) {
        $scope.Query = '';
        $scope.OrderListLoaded = false;
        $scope.orders = [];

        OrderService.getOrderList({}, function (err, res) {
            if (!err) {
                $scope.orders = res;
                $scope.orderListLoaded = true;
            } else {
                Materialize.toast('Произошла ошибка: ' + err.message, 5000, 'red');
            }
        });

        function changeOrderState(id, newState, date) {
            $scope.orders.forEach(item => {
                if (item._id == id) {
                    item.state = newState;
                    item.changed = date;
                }
            });
        };

        $scope.changeState = function (id, newState) {
            OrderService.changeState({"_id": id, "state": newState}, function (err, res) {
                if (!err) {
                    changeOrderState(id, res.state, res.changed);
                    Materialize.toast('Изменено состояние заказа', 3000);
                } else {
                    Materialize.toast('Произошла ошибка: ' + err.message, 5000, 'red');
                }
            });
        };

        $scope.isNew = function (order) {
            let date = new Date();
            date.setMinutes(date.getMinutes() - 5);
            let dateTime = date.getTime();
            return (Date.parse(order.ordered) >= dateTime || Date.parse(order.changed) >= dateTime)
        };
    });