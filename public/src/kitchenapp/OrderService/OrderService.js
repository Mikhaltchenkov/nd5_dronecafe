'use strict';

angular
    .module('KitchenApp')
    .factory('OrderService', function(SocketService) {

        let orders = [];
        SocketService.on('order:new', function (data) {
            orders.push(data);
        });
        return {
            getOrderList(req, cb) {
                SocketService.emit('orders:get', req, function (res) {
                    if (res.code == 200) {
                        orders = res.result;
                        cb(undefined, orders);
                    } else {
                        cb(res);
                    }
                });
            },
            changeState(req, cb) {
                SocketService.emit('orders:change', req, function (res) {
                    if (res.code == 200) {
                        cb(undefined, res.result);
                    } else {
                        cb(res);
                    }
                });
            }
        }
    });