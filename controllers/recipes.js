const mongoose = require('mongoose');
const Recipes = mongoose.model('Recipes');

/**
 * Вывод списка блюд
 * @param req - [ {"filter": [ id1, id2, id3, ... ]} ] - необязательное значение, фильтр по перечисленным id блюд
 * @param fn - callback
 */

module.exports.list = function (req, fn) {
    let filter = {};
    if (req && req.filter) {
        let ids = req.filter.map(el => mongoose.Types.ObjectId(el));
        filter = {"_id": {$in: ids}};
    }
    Recipes.find(filter).exec().then(
        recipes => {
            fn({'code': 200, result: recipes});
        },
        err => {
            fn({'code': 404, 'message': err.message});
        }
    );
};

/**
 * Ищет в БД блюдо с указанным id
 * @param req - {"_id": recipeId }
 * @param fn - callback
 */
module.exports.getOne = function (req, fn) {
    Recipes.findById(req._id).exec().then(
        recipe => {
            fn({'code': 200, result: recipe});
        },
        err => {
            fn({'code': 404, 'message': err.message});
        }
    );
};
