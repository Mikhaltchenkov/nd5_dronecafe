const Clients = require('./clients');

const users = {};

/**
 * Создание подключения пользователя в On-Line для реализации обратной связи
 * @param id - пользователя
 * @param type - тип пользователя "client" или "kitchen"
 * @param socket - объект socket.io
 * @returns {*}
 */
module.exports.add = addUser = function (id, type, socket) {
    users[id] = {"id": id, "type": type, "socket": socket};
    return users[id];
};

/**
 * Удаление подключения пользователя
 * @param socketId - идентификатор подключения
 */
module.exports.delete = function (socketId) {
    Object.keys(users).forEach(key => {
        if (users[key].socket.id === socketId) {
            delete users[key];
        }
    });
};

/**
 * Перечисление пользователей определенного типа
 * @param type - "client" или "kitchen"
 * @returns {*}
 */
module.exports.listByType = function (type) {
    return Object.keys(users).reduce((res, key) => (users[key].type === type) ? res.concat(users[key]) : res, []);
};

/**
 * Возврат пользователя по id
 * @param id
 * @returns {*}
 */
module.exports.get = function (id) {
    return users[id];
};

/**
 * Возвращает количество пользователей в On-Line
 * @returns {Number}
 */
module.exports.count = countUsers = function () {
    return Object.keys(users).length;
};

/**
 * Отправка сообщения пользователю
 * @param id
 * @param type
 * @param data
 */
module.exports.sendMessage = function (id, type, data) {
    if (users[id]) {
        users[id].socket.emit(type, data);
    }
};

/**
 * Отправка сообщения пользователям типа kitchen
 * @param messageType
 * @param data
 */
module.exports.sendMessageToKitchen = function (messageType, data) {
    Object.keys(users).forEach(key => {
        if (users[key].type === 'kitchen') {
            users[key].socket.emit(messageType, data);
        }
    });
};

/**
 * Обработка события логирования пользователя
 * @param user
 * @param fn
 * @param socket
 */
module.exports.loginUser = function (user, fn, socket) {
    Clients.find(user.email, (res) => {
        if (res.code === 200) {
            console.log(socket.id + ' Founded client with id: ' + res.result._id);
            fn(res);
            addUser(res.result._id, 'client', socket);
            console.log('Users count: ' + countUsers());
        } else {
            Clients.add(user, (res) => {
                console.log(socket.id + ' Created new client with id: ' + res.result._id);
                fn(res);
                addUser(res.result._id, 'client', socket);
                console.log('Users count: ' + countUsers());
            });
        }
    });
};

module.exports.loginKitchenUser = function(fn, socket) {
    addUser(null, 'kitchen', socket);
    fn({"code": 200});
};

