const mongoose = require('mongoose');
const Clients = mongoose.model('Clients');
const Users = require('./users');

/**
 * Возвращает список клиентов в БД
 * @param req
 * @param fn - callback
 */
module.exports.list = function (req, fn) {
    Clients.find().exec().then(
        clients => {
            fn({'code': 200, result: clients});
        },
        err => {
            fn({'code': 404, 'message': err.message});
        }
    );
};

/**
 * Возвращает клиента с указанным id
 * @param req - {"_id": clientId}
 * @param fn - callback с результатом
 */
module.exports.getOne = function (req, fn) {
    Clients.findById(req._id).exec().then(
        client => {
            fn({'code': 200, result: client});
        },
        err => {
            fn({'code': 404, 'message': 'Object not found'});
        }
    );
};

/**
 * Изменяет баланс клиента на указанную сумму. При изменении отправляет уведомление клиенту с суммой на балансе
 * @param req - {"_id": clientId, "sum": summa }
 * @param fn - callback
 */
module.exports.changeBalance = function (req, fn) {
    Clients.findById(req._id, (err, client) => {
        if (client) {
            console.log('[changeBalance] ' + req._id + ' += ' + req.sum);
            client.balance += req.sum;
            client.save().then(
                client => {
                    fn({'code': 200, result: client});
                    Users.sendMessage(client._id, 'balance:changed', client.balance);
                },
                err => {
                    fn({'code': 500, 'message': err.message});
                }
            );
        } else {
            fn({'code': 404, 'message': 'Object not found'});
        }
    });
};

/**
 * Поиск клиента в БД по e-mail
 * @param req - e-mail
 * @param fn - callback
 */
module.exports.find = function (req, fn) {
    Clients.findOne({'email': req}).then(
        client => {
            if (!client) {
                fn({'code': 404, 'message': 'Object not found'});
            } else {
                fn({'code': 200, result: client});
            }
        },
        err => {
            fn({'code': 404, 'message': 'Object not found'});
        }
    );
};

/**
 * Добавление клиента в БД
 * @param req - { "name": clientName, "email": clientEmail }
 * @param fn - callback
 */
module.exports.add = function (req, fn) {
    Clients.create({
        'name': req.name,
        'email': req.email,
        'balance': 100
    }).then(
        client => {
            fn({'code': 201, result: client});
        },
        err => {
            fn({'code': 400, 'message': err.message});
        }
    );
};
