const mongoose = require('mongoose');
const Orders = mongoose.model('Orders');
const Drones = require('netology-fake-drone-api');
const Users = require('./users');
const Recipes = require('./recipes');
const Clients = require('./clients');

/**
 * Вывод списка заказов
 * @param req - undefined или {"client": _id} в случае необходимости фильтрации по клиенту
 * @param fn - callback
 */
module.exports.list = function (req, fn) {
    let query = {};
    if (req.client) {
        query = {'client': mongoose.Types.ObjectId(req.client)};
    }
    Orders.find(query).populate('client recipe').exec().then(
        orders => {
            fn({'code': 200, result: orders});
        },
        err => {
            fn({'code': 404, 'message': err.message});
        }
    );
};

/**
 * Добавление заказа в БД
 * @param req - объект заказа вида {"client": clientId, "recipe": recipeId}
 * @param fn - callback
 */
module.exports.add = addOrder = function (req, fn) {
    Orders.create({
        'client': mongoose.Types.ObjectId(req.client),
        'recipe': mongoose.Types.ObjectId(req.recipe),
        'ordered': Date.now(),
        'state': 0
    }).then(
        order => {
            Orders.populate(order, {path: "client recipe"}).then(
                order => {
                    fn({"code": 201, "result": order});
                    Users.sendMessageToKitchen('order:new', order);
                },
                err => {
                    fn({'code': 400, 'message': err.message});
                }
            );
        },
        err => {
            fn({'code': 404, 'message': err.message});
        }
    );
};


/**
 * Отправление сообщения об изменении статуса заказа клиенту, заказавшему заказ
 * @param id - id клиента
 * @param orderId - id заказа
 */
function changeOrderEventToUser(id, orderId) {
    Orders.findById(orderId).populate('client recipe').exec().then(
        order => {
            Users.sendMessage(id, 'order:changed', order);
        }
    );
}

/**
 * Удаление заказа через 2 минуты после изменения статуса на "Доставлено" или "Возникли трудности"
 * @param order - объект заказа
 */
function deleteOrder(order) {
    setTimeout(() => {
        Orders.findByIdAndRemove(order._id).then(
            res => {
                console.log('[deleteOrder] Заказ ' + order._id + ' удален по таймауту.');
                Users.sendMessage(order.client, 'order:deleted', order._id)
            },
            err => {
                console.log('[deleteOrder] При удалении заказа ' + order._id + ' произошла ошибка: ' + err.message);
            }
        );
    }, 120000);
}

/**
 * Изменение статуса заказа
 * @param req - объект заказа
 * @param fn - callback
 */
module.exports.changeState = function (req, fn) {
    if (req.state === 1 || req.state === 2) {
        Orders.findById(req._id).exec().then(
            order => {
                if (order) {
                    order.state = req.state;
                    order.changed = Date.now();
                    order.save().then(
                        order => {
                            fn({"code": 200, "result": order});
                            changeOrderEventToUser(order.client, order._id);
                            //Иммитация доставки
                            if (req.state === 2) {
                                Drones.deliver(order.client, order.recipe).then(() => {
                                    console.log('Доставка заказа ' + order._id + ' успешно завершена.');
                                    order.state = 4;
                                    order.changed = Date.now();
                                    order.save().then(
                                        order => {
                                            changeOrderEventToUser(order.client, order._id);
                                        });
                                    deleteOrder(order);
                                }, () => {
                                    console.log('При доставке заказа ' + order._id + ' возникли проблемы.');
                                    order.state = 3;
                                    order.changed = Date.now();
                                    order.save().then(
                                        order => {
                                            changeOrderEventToUser(order.client, order._id);
                                        });
                                    deleteOrder(order);
                                });
                            }
                        },
                        err => {
                            fn({"code": 500, "message": err.message});
                        }
                    );
                } else {
                    fn({"code": 404, "message": "Object not found"});
                }
            },
            err => {
                fn({"code": 500, "message": err.message});
            }
        );
    } else {
        fn({"code": 400, "message": "Bad request"});
    }
};

/**
 * Обработка события оформления заказа
 * @param request
 * @param fn
 */
module.exports.postOrder = function (request, fn) {
    Clients.getOne({'_id': request.client}, function (res) {
        if (res.code === 200) {
            let arrayOfRecipes = request.items.reduce((arr, el) => arr.concat(el.id), []);
            Recipes.list({"filter": arrayOfRecipes}, function (recipes) {
                if (recipes.code === 404) {
                    console.log(request.client + ' ' + recipes.message);
                    fn(recipes);
                } else {
                    let orderSum = 0;
                    for (let i = 0; i < request.items.length; i++) {
                        let recipe = recipes.result.find(el => el._id == request.items[i].id);
                        if (recipe) {
                            orderSum += recipe.price * request.items[i].quantity;
                        } else {
                            console.log(`Блюдо ${request.items[i].id} не найдено!`);
                            fn({'code': 404, 'message': `Блюдо ${request.items[i].id} не найдено!`});
                            orderSum = -1;
                            break;
                        }
                    }
                    if (orderSum > 0) {
                        if (orderSum > res.result.balance) {
                            console.log(request.client + ' Не достаточно средств на счету');
                            fn({'code': 400, 'message': 'Не достаточно средств на счету'});
                        } else {
                            Clients.changeBalance({
                                "_id": res.result._id,
                                "sum": orderSum * -1
                            }, function (client) {
                                if (client.code !== 200) {
                                    console.log(request.client + ' ' + client.message);
                                    fn(client);
                                } else {
                                    request.items.forEach(el => {
                                        for (let i = 0; i < el.quantity; i++) {
                                            addOrder({
                                                'client': request.client,
                                                'recipe': el.id
                                            }, function (res) {
                                                console.log('Заказ оформлен - ' + res.code + ' ' + res.result.id);
                                            });
                                        }
                                    });
                                    console.log(request.client + 'Заказ оформлен');
                                    fn({'code': 200, 'message': 'Заказ оформлен'});
                                }
                            });
                        }
                    }
                }
            });
            //OK
        } else {
            fn({'code': 400, 'message': 'Не корректный ID пользователя'});
        }
    });
};

