const mongoose = require('mongoose');

var clientSchema = new mongoose.Schema({
    name: String,
    email: String,
    balance: Number
});
mongoose.model('Clients', clientSchema);