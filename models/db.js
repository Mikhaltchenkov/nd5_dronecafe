const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
if (!process.env.MLAB_URI) {
    throw new Error("Process environment variable MLAB_URI is not set!");
}
mongoose.connect(process.env.MLAB_URI, {useMongoClient: true}).then(
    () => {
        console.log('MongoDB connection OK.');
    },
    error => {
        throw new Error('MongoDB connection error: ' + error);
    }
);

require('./clients');
require('./recipes');
require('./orders');
