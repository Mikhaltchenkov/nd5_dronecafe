const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var orderSchema = new mongoose.Schema({
    client: { type: Schema.Types.ObjectId, ref: 'Clients' },
    recipe: { type: Schema.Types.ObjectId, ref: 'Recipes' },
    ordered: Date,
    changed: Date,
    state: Number,
    ended: Date
});

mongoose.model('Orders', orderSchema);