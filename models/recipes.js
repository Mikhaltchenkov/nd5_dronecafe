'use strict';

const mongoose = require('mongoose');
const fs = require('fs');

var recipeSchema = new mongoose.Schema({
    title: String,
    image: String,
    rating: Number,
    rateNumber: Number,
    id: Number,
    ingredients: Array,
    price: Number
});
mongoose.model('Recipes', recipeSchema);

let Recipes = mongoose.model('Recipes');
Recipes.find().exec((err,recipes) => {
    if (!err) {
        if (recipes.length == 0) {
            console.log('Import recipes into MongoDB from file...');
            fs.readFile('./models/menu.json', 'utf8', function (err, contents) {
                if (!err) {
                    let count =0;
                    try {
                        let recipeList = JSON.parse(contents);
                        recipeList.forEach(el => {
                            el.rateNumber = 0;
                            if (el.rating>0) {
                                el.rateNumber = Math.ceil(el.rating / (Math.random() * 3 + 2));
                            }
                            Recipes.create(el, (err, item) => {
                                console.log(el.title);
                                if (err) {
                                    console.log('Error: ' + el.id + ' ' + err.message);
                                } else {
                                    console.log('OK');
                                    count++;
                                }
                            });
                        });
                    } catch (err) {
                        console.log('Error: ' + err.message);
                    }
                    console.log(`Imported ${count} items.`)
                } else {
                    console.log('Error: ' + err.message);
                }
            });
        }
    }
});